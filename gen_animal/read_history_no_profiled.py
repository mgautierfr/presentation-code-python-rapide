#!/usr/bin/env python3

import re

cats = {}

class Cat:
    def __init__(self, date, name, gender, color, pattern, mother, pfathers):
        self.birth_date = date
        self.name = name
        self.gender = gender
        self.color = color
        self.pattern = pattern
        self.mother = mother
        self.pfathers = pfathers

    def age(self, date):
        return date-self.birth_date

def parse_born_line(line):
    return re.search(r"(?P<name>.+) \((?P<gender>\w+), (?P<color>\w+), (?P<pattern>\w+)\) from (?P<mother>.+) \((?P<mother_age>\d+)\)", line).groupdict()


def parse_death_line(line):
    return re.search(r"(?P<name>.+) at (?P<age>\d+)", line).groupdict()

def get_potential_fathers(color, pattern):
    pfathers = []
    for candidats in cats.values():
        for candidat in candidats:
            if candidat.gender != 'male':
                continue
            if candidat.color == color and candidat.pattern == pattern:
                pfathers.append(candidat)
    return pfathers


def main():
    with open('event.txt', 'r') as f:
        for line in f:
            date, what, who = line.split(':')
            date = int(date)

            if what == 'BORN':
                info = parse_born_line(who)
                if info['mother'] == 'None':
                    info['mother'] = None
                else:
                    mothers = cats[info['mother']]
                    for cat in mothers:
                        if cat.age(date) == int(info['mother_age']):
                            info['mother'] = cat
                            break

                pfathers = get_potential_fathers(info['color'], info['pattern'])
                del info['mother_age']

                cat = Cat(date, pfathers=pfathers, **info)
                if cat.name in cats:
                    cats[cat.name].append(cat)
                else:
                    cats[cat.name] = [cat]


            if what == 'DEATH':
                info = parse_death_line(who)
                named_cats = cats[info['name']]
                death_cat = None
                for death_cat in named_cats:
                    if death_cat.age(date) == int(info['age']):
                        break
                named_cats.remove(death_cat)

if __name__ == "__main__":
    main()

nb_cats = sum(len(nameds) for nameds in cats.values())
print(nb_cats)
