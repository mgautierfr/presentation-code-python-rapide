class: title

# Trucs et Astuces pour écrire du code plus rapide

---
class: title

# Qui suis-je ?

---

# Qui suis-je ?

[Matthieu Gautier](http://mgautier.fr).  
Développeur indépendant chez Kymeria.

---

# Pour qui ?

--

Accessible à tout le monde (j'espère, je vais essayer).

Enfin, il faut connaitre python.

Mais destiné aux "non-débutants".  
Parce que l'optimisation, c'est une fois que ça marche.


---

# Testez !

Tout le temps. Aucunes assomptions.

--

Profile, cProfile

line_profiler

timeit

---

# Connaissez vos structures de données

Et les algorithmes qui vont avec.

- list (tableau)

--

- collections.deque (liste)

--

- set (ensemble)

-- 

- dict (association clé/valeur)

---

# list

| Operation | Average Case | Amortized Worst Case |
|:---------:|:------------:|:--------------------:|
|Copy |  O(n) | O(n)
|Append | O(1) | O(1)
|Insert | O(n) | O(n)
|Get Item | O(1) | O(1)
|Set Item | O(1) | O(1)
|Delete Item | O(n) | O(n)
|Iteration | O(n) | O(n)
|Get Slice | O(k) | O(k)
|Del Slice | O(n) | O(n)
|Set Slice | O(k+n) | O(k+n)
|Extend | O(k) | O(k)
|Sort | O(n log n) | O(n log n)
|Multiply | O(nk) | O(nk)
|x in s | O(n) |  |
|min(s), max(s) | O(n) |  |
|Get Length | O(1) | O(1) 

---

# collection.deque

| Operation | Average Case | Amortized Worst Case |
|:---------:|:------------:|:--------------------:|
|Copy | O(n) | O(n)
|append | O(1) | O(1)
|appendleft | O(1) | O(1)
|pop | O(1) | O(1)
|popleft | O(1) | O(1)
|extend | O(k) | O(k)
|extendleft | O(k) | O(k)
|rotate | O(k) | O(k)
|remove | O(n) | O(n) 


---

# set

| Operation | Average Case | Amortized Worst Case |
|:---------:|:------------:|:--------------------:|
|x in s | O(1) | O(n)
|Union s&#124;t | O(len(s)+len(t))
|Intersection s&t | O(min(len(s), len(t)) | O(len(s) * len(t))
|Multiple intersection s1&s2&..&sn |  | (n-1)*O(l) </br>l = max(len(s1),..,len(sn))
|Difference s-t | O(len(s))
|s.difference_update(t) | O(len(t))
|Symmetric Difference s^t | O(len(s)) | O(len(s) * len(t))
|s.symmetric_difference_update(t) | O(len(t)) | O(len(t) * len(s))

---

# dict

| Operation | Average Case | Amortized Worst Case |
|:---------:|:------------:|:--------------------:|
|Copy | O(n) | O(N)
|Get Item | O(1) | O(n)
|Set Item | O(1) | O(n)
|Delete Item | O(1) | O(n)
|Iteration | O(n) | O(N)

---


Quel est le container le plus rapide à créer ?


---

Quel est le container le plus rapide à créer ?

- tuple
```python
c = 0, 1```


---

Quel est le container le plus rapide à créer ?

- tuple
- list
```python
c = [0, 1]```

---

Quel est le container le plus rapide à créer ?

- tuple
- list
- dict
```python
c = {'arg1':0, 'arg2':1}```


---

Quel est le container le plus rapide à créer ?

- tuple
- list
- dict
- namedtuple
```python
from collections import namedtuple
NamedTuple = namedtuple('NamedTuple', 'arg1 arg2')
c = NameTuple(0, 1)```

---

Quel est le container le plus rapide à créer ?

- tuple
- list
- dict
- namedtuple
- obj
```python
class Obj:
    def __init__(self, arg1, arg2):
        self.arg1 = arg1
        self.arg2 = arg2
c = Obj(0, 1)```


---

Quel est le container le plus rapide à créer ?

- tuple
- list
- dict
- namedtuple
- obj
- obj avec des __slots__
```python
class Obj:
    __slots__ = ('arg1', 'arg2')
    def __init__(self, arg1, arg2):
        self.arg1 = arg1
        self.arg2 = arg2
c = Obj(0, 1)```

---

Quel est le container le plus rapide à créer ?

- tuple
- list
- dict
- namedtuple
- obj
- obj avec des __slots__

![Struct Creation](stuct_creation.svg)

---

Quel est le container le plus rapide à **lire** ?

- tuple
- list
- namedtuple

```python
c[0] + c[1]```

---

Quel est le container le plus rapide à **lire** ?

- dict

```python
c['arg1'] + c['arg2']```

- obj
- obj avec des __slots__
- namedtuple attributes

```python
c.arg1 + c.arg2```

---

Quel est le container le plus rapide à **lire** ?

- tuple
- list
- namedtuple
- dict
- obj
- obj avec des __slots__
- namedtuple attributes

![Struct Read](stuct_read.svg)

---

Quel est le container le plus rapide à écrire et lire ?

- tuple
- list
- dict
- namedtuple
- obj
- obj avec des __slots__

--

![Struct create and read](stuct_creation_read.svg)

---

# Les appels de fonctions

Les appels de fonctions les plus rapides sont ceux qu'on ne fait pas

---

Soit :

```python
def function(arg1=10, arg2=20, arg3=30): pass```

L'appel le plus rapide entre:

---

Soit :

```python
def function(arg1=10, arg2=20, arg3=30): pass```

L'appel le plus rapide entre:

- par défaut
```python
function()```

Soit :

```python
def function(arg1=10, arg2=20, arg3=30): pass```

L'appel le plus rapide entre:

- par défaut
- direct
```python
function(1, 2 , 3)```


---

Soit :

```python
def function(arg1=10, arg2=20, arg3=30): pass```

L'appel le plus rapide entre:

- par défaut
- direct
- kwargs
```python
function(arg1=1, arg2=2 , arg3=3)```



---

Soit :

```python
def function(arg1=10, arg2=20, arg3=30): pass```

L'appel le plus rapide entre:

- par défaut
- direct
- kwargs 
- kwargs inversé
```python
function(arg3=3, arg2=2 , arg1=3)```


---

Soit :

```python
def function(arg1=10, arg2=20, arg3=30): pass```

L'appel le plus rapide entre:

- par défaut
- direct
- kwargs 
- kwargs inversé
- *list
```python
l = [1, 2, 3]
function(*l)```


---

Soit :

```python
def function(arg1=10, arg2=20, arg3=30): pass```

L'appel le plus rapide entre:

- par défaut
- direct
- kwargs 
- kwargs inversé
- *list
- **mapping
```python
d = {'arg1':1, 'arg2':2, 'arg3':3}
function(**l)```

---

L'appel le plus rapide entre:

- par défaut
- direct
- kwargs 
- kwargs inversé
- *list
- **mapping

![Function call](function_call1.svg)

---

Combien de temps pour :

```python
function(1, arg3=3)```

--

![Function call](function_call2.svg)

---

On peut remplacer par :

- direct
```python
function(1, 10, 3)```

- direct avec default
```python
function(1, function.__defaults__[1], 3)```

--

![Function call](function_call3.svg)

---

On peut remplacer par :

- direct
```python
function(1, 10, 3)```

- direct avec default
```python
function(1, function.__defaults__[1], 3)```

- direct avec default 2
```python
d =  function.__defaults__[1]
function(1, d, 3)```

---

On peut remplacer par :

- direct
- direct avec default
- direct avec default 2

![Function call](function_call4.svg)

---

# Machin comprehension

Une manière plus efficace d'écrire :

```python
machin = []
for elem in input_iterable:
    if want_elem(elem):
        truc.append("truc avec {}".format(elem))
```

--

Ça devient :

```python
machin = ["truc avec {}".format(elem) for elem in input_iterable if want_elem(elem)]
```

---

# Machin comprehension

Une manière plus efficace d'écrire :

```python
machin = []
for elem in input_iterable:
    if want_elem(elem):
        machin.append("truc avec {}".format(elem))
```

Ça devient :

```python
machin = ["truc avec {}".format(elem)
          for elem in input_iterable
          if want_elem(elem)
         ]
```

---

# Machin comprehension

Ça marche avec des `set`:

```python
machin = {"truc avec {}".format(elem)
          for elem in input_iterable
          if want_elem(elem)
         }
```

--

Avec des `dict`:

```python
machin = {elem[0]:elem[1]
          for elem in input_iterable
          if want_elem(elem)
         }
```

--

Avec des générateurs:

```python
machin = (elem[0]:elem[1]
          for elem in input_iterable
          if want_elem(elem)
         )
```

---

Et c'est plus rapide:

```python
machin = []
for elem in range(1000000):
    if elem % 2:
        machin.append(elem*3)
```

vs 

```python
machin = [elem*3 for elem in range(1000000) if elem%2]
```

![Comprehension](comprehension.svg)

---

Et c'est genre **vraiment** plus rapide:

```python
machin = []
for elem in range(1000000):
    if elem:
        machin.append(elem)
```

vs 

```python
machin = [elem for elem in range(1000000) if elem]
```

![Comprehension](comprehension2.svg)


---

# Dictionnary lookup


Soit :

```python
d = {i:i*2 for i in range(1000)}
k = 5
```

Quel est le plus rapide :

- d.get
```python
v = d.get(k, [])
```

---

# Dictionnary lookup


Soit :

```python
d = {i:i*2 for i in range(1000)}
k = 5
```

Quel est le plus rapide :

- d.get
- try except
```python
    try:
        v = d[k]
    except KeyError:
        v = []
```

---

# Dictionnary lookup


Soit :

```python
d = {i:i*2 for i in range(1000)}
k = 5
```

Quel est le plus rapide :

- d.get
- try except
- try except pass
```python
    v = []
    try:
        v = d[k]
    except KeyError:
        pass
```

---

# Dictionnary lookup


Soit :

```python
d = {i:i*2 for i in range(1000)}
k = 5
```

Quel est le plus rapide :

- d.get
- try except
- try except pass
- if:
```python
    if k in d:
        v = d[k]
    else:
        v = []
```

---

# Dictionnary lookup


Soit :

```python
d = {i:i*2 for i in range(1000)}
k = 5
```

Quel est le plus rapide :

- d.get
- try except
- try except pass
- if:
- ifelse
```python
v = d[k] if k in d else []
```

---

# Dictionnary lookup

- d.get
- try except
- try except pass
- if:
- ifelse

![Comprehension](dict_access1.svg)

---

# Dictionnary lookup

Avec `None` au lieu de `[]` ?

--

![Comprehension](dict_access2.svg)

---

# Dictionnary lookup

Et avec `k = 5000` ?

--

![Comprehension](dict_access3.svg)

---

# Default dict

```python
    d = {}
    for i in range(1000000):
        k = i%10
        try:
            d[k].append(v)
        except KeyError:
            d[k] = [v]
```

vs 

```python
    from collections import defaultdict
    d = defaultdict(list)
    for i in range(1000000):
        k = i%10
        d[k].append(v)
```

---

# Default dict

![Comprehension](defaultdict.svg)

---

# Remap method

```python
class Container:
    def __init__(self):
        self.l = []
    
    def add_value(self, value):
        if value == 0:
            self.l.append("Oups")
        else:
            self.l.append(value)

container = Container()

for i in range(1000000):
    container.add_value(i)```

---

# Remap method

```python
class Container:
    def __init__(self):
        self.l = []
    
    def add_value(self, value):
        self.l.append("Oups")
        self.add_value = self._add_other_values
    
    def _add_other_values(self, value):
        self.l.append(value)

container = Container()

for i in range(1000000):
    container.add_value(i)```

---

# Remap method

![Comprehension](normal.svg)

---

#Demo


???

- Use list comprehension in get_potential_father
- Use a container for potential fathers (with alive property) and if for check
- Use alive attribute
- Remove dead fathers from container
- remove if and use try
- No build of no father
- compile regex
- No dict in parse
- reduce .access
- no class
- else instead of if
- no function call
- next instead of forloop

---

class: title

# Questions ?

---

# Informations diverses

Cette présentation est sous licence [CC-BY-SA](http://creativecommons.org/licenses/by-sa/3.0/fr/).
